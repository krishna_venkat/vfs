#ifndef FILESYSTEMCOMPONENT_HPP
#define FILESYTEMCOMPONENT_HPP

#include <iostream>
#include <memory>
#include <vector>

#include "ComponentType.hpp"

class FileSystemComponent {
public:
    FileSystemComponent(std::string);
    ~FileSystemComponent();

    std::shared_ptr<FileSystemComponent>              parent;
    std::vector<std::shared_ptr<FileSystemComponent>> child;
    std::string                                       name;
    std::string                                       absolutePath;
    enum ComponentType                                componentType;
};

#endif