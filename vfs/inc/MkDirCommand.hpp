#include "Command.hpp"
#include <vector>

class MkDirCommand : public ICommand {
public:
    void executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem);
};