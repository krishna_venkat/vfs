#include "../inc/RmFileCommand.hpp"

#include <bits/stdc++.h>

#include <iostream>
#include <string>

bool compareComponents(std::shared_ptr<FileSystemComponent> a, std::shared_ptr<FileSystemComponent> b) {
    return (a->name.compare(b->name) < 0);
}

bool compareString(std::string a, std::string b){
    return (a.compare(b) < 0);
}

void RmFileCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem) {

    // ? Should I do the error handling here or after declaring the variables?
    if (arguments.size() < 2) {
        cout << "rmdir: missing operand" << endl;
        return;
    }

    std::vector<std::shared_ptr<FileSystemComponent>> components = filesystem->currentWorkingDir->child;

    //TODO: Rename or find a better solution
    std::vector<std::string> larguments                     = arguments;
    bool isFileFound = false;

    std::sort(components.begin(), components.end(), compareComponents);
    std::sort(larguments.begin(), larguments.end(), compareString);
    for (auto i : components) {
        cout << i->name << endl;
    }

    for (auto i : larguments) {
        cout << i << endl;
    }


    // ! Rewrite Algo to reduce the complexity.
    for (int j = 0; j < larguments.size();) {
        for (int i = 0; i < components.size();) {
            if (components[i]->name == larguments[j]) {
                if (components[i]->componentType != _FILE) {
                    cout << larguments[j] << " is not a file" << endl;
                    continue;
                };

                isFileFound = true;
                components.erase(components.begin() + i);
                cout << larguments[j] << " removed " << endl;
            } else if (components[i]->name.compare(larguments[j]) < 0) {
                i++;
            }
            else {
                j++;
            }
        }
        if (!isFileFound) {
            cout << arguments[j] << " not found" << endl;
        }
    }
}