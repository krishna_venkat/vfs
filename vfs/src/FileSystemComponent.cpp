#include "FileSystemComponent.hpp"

#include <iostream>

FileSystemComponent::FileSystemComponent(std::string name) {
    this->name = name;
    std::cout << "File system component construction" << std::endl;
}

FileSystemComponent::~FileSystemComponent() {
    std::cout << "FileSystemComponent destroyed..." << std::endl;
}
