#include "FileSystem.hpp"
#include <iostream>

FileSystem::FileSystem() {
	rootDir = std::make_shared<FileSystemComponent>("root");
    rootDir->parent = 0;
    currentWorkingDir = rootDir;
    rootDir->absolutePath = rootDir->name;
}

FileSystem::~FileSystem() {
    cout << "FileSystem destruction" << std::endl;
}