#include "../inc/Command.hpp"

#include <iostream>
#include <memory>
#include <vector>

#include "../inc/ChangeDirCommand.hpp"
#include "../inc/FileCommand.hpp"
#include "../inc/FileSystem.hpp"
#include "../inc/FindDirCommand.hpp"
#include "../inc/HelpCommand.hpp"
#include "../inc/ListDirCommand.hpp"
#include "../inc/MkDirCommand.hpp"
#include "../inc/RmDirCommand.hpp"
#include "../inc/RmFileCommand.hpp"

using namespace std;

map<string, ICommand*> Map{
    {"mkdir", new MkDirCommand()},
    {"ls", new ListDirCommand()},
    {"find", new FindDirCommand()},
    {"rmdir", new RmDirCommand()},
    {"rmfile", new RmFileCommand()},
    {"file", new FileCommand()},
    {"cd", new ChangeDirCommand()},
    {"help", new HelpCommand()}};
