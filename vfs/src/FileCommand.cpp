#include "../inc/FileCommand.hpp"

void FileCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem) {
    if(arguments.size()<2){
        cout << "file: missing operand" << endl;
        return;
    }

    for (uint16_t i = 1; i < arguments.size();  i++) {
        std::shared_ptr<FileSystemComponent> file = std::make_shared<FileSystemComponent>(arguments[i]);
        
        file->parent = filesystem->currentWorkingDir;
        file->absolutePath = file->parent->absolutePath + "/" + file->name;
        file->componentType = _FILE;

        filesystem->currentWorkingDir->child.push_back(file);
    }
}