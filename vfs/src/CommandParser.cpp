
#include "CommandParser.hpp"

#include <sstream>
#include <vector>

#include "Command.hpp"

CommandParser::CommandParser() {
    cout << "Command Parser Construction" << endl;
}

CommandParser::~CommandParser() {
    cout << "Command Parser Destruction" << endl;
}

void CommandParser::parseCommand(std::string command) {
    std::vector<std::string> commandVector;
    std::istringstream       iss(command);

    for (std::string s; iss >> s;) {
        commandVector.push_back(s);
    }

    if(Map.count(commandVector[0])==0){
        cout << "\"" << commandVector[0] << "\" not found" << endl;
        return;
    }
    
    Map[commandVector[0]]->executeCommand(commandVector, fileSystem_);
}

void CommandParser::setFileSystem(shared_ptr<FileSystem> filesystem){
    fileSystem_ = filesystem;
}