#include "../inc/MkDirCommand.hpp"
#include "Command.hpp"

void MkDirCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem) {
    
    if(arguments.size()<2){
        cout << "mkdir: missing operand" << endl;
        return;
    }

    for (uint16_t i = 1; i < arguments.size(); i++) {
        std::shared_ptr<FileSystemComponent> directory = std::make_shared<FileSystemComponent>(arguments[i]);

        directory->parent = filesystem->currentWorkingDir;
        directory->absolutePath = directory->parent->absolutePath + "/" + directory->name;
        directory->componentType = DIRECTORY;

        filesystem->currentWorkingDir->child.push_back(directory);
    }
}
