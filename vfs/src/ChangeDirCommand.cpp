#include "../inc/ChangeDirCommand.hpp"

void ChangeDirCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem) {
    #define CHILD filesystem->currentWorkingDir->child

    typedef vector<shared_ptr<FileSystemComponent>> IT;

    if(arguments.size()>2){
        cout << "cd : too many arguments" << endl;
        return;
    }

    if(arguments[1] == "/"){
        filesystem->currentWorkingDir = filesystem->rootDir;
        return;
    }

    for(int i=0; i< CHILD.size(); i++){
        if(CHILD[i]->name == arguments[1]){
            if(CHILD[i]->componentType != DIRECTORY){
                cout << arguments[1] << " is not a directory" << endl;
                return;
            }
            filesystem->currentWorkingDir = CHILD[i];
            return;
        }
    }
    cout << arguments[1] << " not found" << endl;
}