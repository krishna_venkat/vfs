
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../../vfs/inc/Command.hpp"
#include "../../vfs/inc/CommandParser.hpp"
#include "../../vfs/inc/FileSystem.hpp"

using namespace testing;

namespace{
class VFSTests : public testing::Test {
    public:
    shared_ptr<ICommandParser> parser; 
    shared_ptr<FileSystem>    fileSystem;
    VFSTests(){
        parser = std::make_shared<CommandParser>();
        fileSystem = std::make_shared<FileSystem>();
    }

    void SetUp(){
        parser->setFileSystem(fileSystem);
    }
};

TEST_F(VFSTests, FileSystemTest){
    EXPECT_EQ(fileSystem->currentWorkingDir->name, "root");
}

TEST_F(VFSTests, CreateDirectoryTest){
    parser->parseCommand("mkdir test");
    EXPECT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test");
}

TEST_F(VFSTests, RemoveDirectoryTest) {
    parser->parseCommand("mkdir test1");
    parser->parseCommand("mkdir test2");
    parser->parseCommand("mkdir test3");
    
    ASSERT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test1");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[1]->name, "test2");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[2]->name, "test3");

    parser->parseCommand("rmdir test1");

    for (auto i: fileSystem->currentWorkingDir->child) {
        EXPECT_NE(i->name, "test1");
    }
}

TEST_F(VFSTests, CreateFileTest){
    parser->parseCommand("file test");
    EXPECT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test");
}

TEST_F(VFSTests, RemoveFileTest) {
    parser->parseCommand("file test1");
    parser->parseCommand("file test2");
    parser->parseCommand("file test3");
    
    ASSERT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test1");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[1]->name, "test2");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[2]->name, "test3");

    parser->parseCommand("rmfile test1");

    for (auto i: fileSystem->currentWorkingDir->child) {
        EXPECT_NE(i->name, "test1");
    }
}

TEST_F(VFSTests, ListFilesTest){
    parser->parseCommand("mkdir test1");
    parser->parseCommand("mkdir test2");
    parser->parseCommand("mkdir test3");
    
    ASSERT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test1");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[1]->name, "test2");
    ASSERT_EQ(fileSystem->currentWorkingDir->child[2]->name, "test3");

    testing::internal::CaptureStdout();
    parser->parseCommand("ls");
    EXPECT_EQ(testing::internal::GetCapturedStdout(), "TYPE\tNAME\n  d\ttest1    \n  d\ttest2    \n  d\ttest3    \n");
}

TEST_F(VFSTests, ChangeDirectoryTest){
    parser->parseCommand("mkdir test1");

    ASSERT_EQ(fileSystem->currentWorkingDir->child[0]->name, "test1");

    parser->parseCommand("cd test1");

    EXPECT_EQ(fileSystem->currentWorkingDir->name, "test1");
}


}   // namespace
